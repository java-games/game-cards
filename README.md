# Game Cards

[![pipeline status](https://gitlab.com/java-games/game-cards/badges/master/pipeline.svg)](https://gitlab.com/java-games/game-cards/commits/master)

## Description

This project provides a small library representing a classical game
of cards.

## Overview

When you want to have a deck of cards, you can use one of the following
factory methods.

```java
Deck thirtyTwo = Deck.thirtyTwoCardDeck();
Deck fiftyTwo = Deck.fiftyTwoCardDeck();
```

Once you have a `deck`, you can perform some classical operations on it.

```java
// Shuffling the deck.
deck.shuffle();

// Cutting the deck.
deck.cut();

// Drawing a card.
Card card = deck.draw();

// Adding back the card.
deck.add(card);
```

When a `card` has been drawn, you can get several informations about it.

```java
Value value = card.getValue();
Suit suit = card.getSuit();
```

The following method gives the URL of an image representing the card, which 
is shipped in the jar of this library.
If you want to use it, you will need to integrate the jar of this library 
into yours.

```java
URL image = card.imageURL();
```

You can find more details about the available classes and their methods by
having a look at the [Javadoc](https://java-games.gitlab.io/game-cards/).

## Dependencies

This library uses the tools provided in the
[Game Tools](https://gitlab.com/java-games/game-tools) project.

