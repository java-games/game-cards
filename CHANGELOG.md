# Changelog

This file reflects the evolution of the Game Cards library, from one version to
another.

## Version 1.0

+ First implementation of the classes used to represent decks and cards
+ Add images representing cards
