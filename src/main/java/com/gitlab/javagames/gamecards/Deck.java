/**
 * Copyright 2017-2018 - Romain WALLON.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.javagames.gamecards;

import java.util.LinkedList;
import java.util.List;

import com.gitlab.javagames.gametools.random.Randomizer;

/**
 * The Deck class represents a deck of game cards.
 *
 * @author Romain WALLON
 * 
 * @version 1.0
 */
public final class Deck {

    /**
     * The cards in this deck.
     */
    private final List<Card> cards;

    /**
     * Creates a new empty Deck.
     */
    private Deck() {
        this.cards = new LinkedList<>();
    }

    /**
     * Creates a new deck of thirty-two cards.
     * 
     * @return The created deck.
     */
    public static Deck thirtyTwoCardDeck() {
        Deck deck = new Deck();
        
        // Creating the cards.
        for (Suit suit : Suit.values()) {
            for (Value value : Value.values()) {
                if (value.isInThirtyTwoCardDeck()) {
                    deck.add(new Card(value, suit));
                }
            }
        }
        
        return deck;
    }

    /**
     * Creates a new deck of fifty-two cards.
     * 
     * @return The created deck.
     */
    public static Deck fiftyTwoCardDeck() {
        Deck deck = new Deck();

        // Creating the cards.
        for (Suit suit : Suit.values()) {
            for (Value value : Value.values()) {
                deck.add(new Card(value, suit));
            }
        }
        
        return deck;
    }

    /**
     * Shuffles this deck of cards.
     */
    public void shuffle() {
        Randomizer.shuffle(cards);
    }

    /**
     * Checks whether there is a card in this deck.
     * 
     * @return If there is a card in this deck.
     */
    public boolean hasCard() {
        return !cards.isEmpty();
    }

    /**
     * Draws a card from this deck.
     * 
     * @return The drawn card, removed from this deck.
     */
    public Card draw() {
        if (hasCard()) {
            return cards.remove(0);
        }
        
        throw new IllegalStateException("Cannot draw a card from an empty deck!");
    }

    /**
     * Cuts this deck.
     */
    public void cut() {
        int heightToCut = Randomizer.randomInteger(cards.size());
        
        for (int i = 0; i < heightToCut; i++) {
            cards.add(draw());
        }
    }

    /**
     * Adds a card to this deck.
     * 
     * @param card The card to add.
     */
    public void add(Card card) {
        cards.add(card);
    }

}
