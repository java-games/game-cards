/**
 * Copyright 2017-2018 - Romain WALLON.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.javagames.gamecards;

import java.net.URL;
import java.util.Objects;

/**
 * The Card class represents a card from a deck.
 * 
 * @author Romain WALLON
 * 
 * @version 1.0
 */
public final class Card {

    /**
     * The value of this card.
     */
    private final Value value;

    /**
     * The suit of this card.
     */
    private final Suit suit;

    /**
     * Creates a new card with the specified value and suit.
     * 
     * @param value The value of the card.
     * @param suit The suit of the card.
     * 
     * @throws NullPointerException If {@code suit} or {@code value} is {@code null}.
     */
    Card(Value value, Suit suit) {
        this.value = Objects.requireNonNull(value);
        this.suit = Objects.requireNonNull(suit);
    }

    /**
     * Gives the suit of this card.
     * 
     * @return The suit of this card.
     */
    public Suit getSuit() {
        return suit;
    }

    /**
     * Gives the value of this card.
     * 
     * @return The value of this card.
     */
    public Value getValue() {
        return value;
    }

    /**
     * Gives the URL of the image representing the card.
     * 
     * @return The URL of this card's image.
     */
    public URL imageURL() {
        return getClass().getResource(String.format("/img/%s_%s.gif", value, suit));
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return value.hashCode() + suit.hashCode();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        Card other = (Card) obj;
        return (value == other.value) && (suit == other.suit);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return value + " of " + suit;
    }

}
