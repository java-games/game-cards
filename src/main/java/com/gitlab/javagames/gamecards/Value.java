/**
 * Copyright 2017-2018 - Romain WALLON.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.javagames.gamecards;

/**
 * The Value enumerates all the possible values that a classical deck of game cards may
 * contain.
 * 
 * @author Romain WALLON
 * 
 * @version 1.0
 */
public enum Value {

    /**
     * The value "Ace".
     */
    ACE(true),

    /**
     * The value "2".
     */
    TWO(false),

    /**
     * The value "3".
     */
    THREE(false),

    /**
     * The value "4".
     */
    FOUR(false),

    /**
     * The value "5".
     */
    FIVE(false),

    /**
     * The value "6".
     */
    SIX(false),

    /**
     * The value "7".
     */
    SEVEN(true),

    /**
     * The value "8".
     */
    EIGHT(true),

    /**
     * The value "9".
     */
    NINE(true),

    /**
     * The value "10".
     */
    TEN(true),

    /**
     * The value "Jack".
     */
    JACK(true),

    /**
     * The value "Queen".
     */
    QUEEN(true),

    /**
     * The value "King".
     */
    KING(true);

    /**
     * If this value is present in a thirty-two-card deck.
     */
    private final boolean inThirtyTwoCardDeck;

    /**
     * Creates a new Value.
     * 
     * @param inThirtyTwoCardDeck If the value is present in a thirty-two-card deck.
     */
    private Value(boolean inThirtyTwoCardDeck) {
        this.inThirtyTwoCardDeck = inThirtyTwoCardDeck;
    }

    /**
     * Checks whether this value is present in a thirty-two-card deck.
     *
     * @return If this value is present in a thirty-two-card deck.
     */
    boolean isInThirtyTwoCardDeck() {
        return inThirtyTwoCardDeck;
    }

}
