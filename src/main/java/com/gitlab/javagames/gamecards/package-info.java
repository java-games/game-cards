/**
 * The com.gitlab.javagames.gamecards package contains some classes used to handle game
 * cards.
 *
 * @author Romain WALLON
 *
 * @version 1.0
 */

package com.gitlab.javagames.gamecards;
